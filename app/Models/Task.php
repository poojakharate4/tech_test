<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as AuditableTrait;

class Task extends Model implements Auditable
{
    use AuditableTrait;
    use HasFactory, SoftDeletes;
    
    protected $fillable = [
        'name',
        'project_id',
        'module_id',
        'sub_module_id',
        'start_date',
        'end_date',
        'type',
        'status',
        'last_operation'
    ];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function module()
    {
        return $this->belongsTo(Module::class);
    }

    public function subModule()
    {
        return $this->belongsTo(SubModule::class);
    }
}

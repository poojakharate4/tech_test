<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Route;
use App\Models\Employee;
use DB;
use Hash;
use Str;

class AdminLoginController extends Controller
{
   
    public function __construct()
    {
      $this->middleware('guest:admin', ['except' => ['logout']]);
    }
    
    public function showLoginForm()
    {
      return view('admin.login');
    }
    
    public function login(Request $request)
    {
        // Validate the form data
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:8'
        ]);

        if ($user = Employee::where('email', $request->email)->role('admin')->first()) 
        {
            if (Hash::check($request->password, $user->password)) 
            {
                Auth::guard('admin')->login($user);
                
                return redirect()->intended(route('admin.dashboard'));
            }
            else
            {
                return back()->with('error','Invalid Login. If you’ve forgotten your password, please use the Forgot Password link.');
            }
        }
        else
        {
            return redirect()->back()->with('error','Sorry, there is no record of an account associated with this email. Please retry.')->withInput($request->only('email', 'remember'));
        }
    }
    
    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('/admin');
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Module;
use App\Models\SubModule;
use App\Models\Project;

use Session;

class SubModuleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $modules = SubModule::get();

        return view('sub-module.index', compact('modules'));
    }

    public function addSubModule(Request $request)
    {
        $projects = Project::get();
        $modules = Module::get();

        return view('sub-module.add', compact('projects', 'modules'));
    }


    public function saveSubModule(Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'project_id' => ['required'],
            'module_id' => ['required'],
        ]);


        SubModule::create($formData);

        return redirect('/sub-modules')->with('success', 'Sub-Module saved successfully!');
    }


    public function editSubModule($id, Request $request)
    {
        $sub_module = SubModule::whereId($id)->first();

        $projects = Project::get();
        $modules = Module::whereProjectId($sub_module->project_id)->get();

        return view('sub-module.edit', compact('projects', 'modules', 'sub_module'));
    }


    public function updateSubModule($id, Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'project_id' => ['required'],
            'module_id' => ['required'],
        ]);

        $formData['last_operation'] = 'E';

        $sub_module = SubModule::whereId($id)->first();

        $sub_module->update($formData);

        return redirect('/sub-modules')->with('success', 'Sub-Module updated successfully!');
    }

    public function deleteSubModule(Request $request)
    {
        $id = $request->id;
        $module = SubModule::whereId($id)->first(); 
        $module->last_operation ='D';
        $module->save();
        
        if ($module->delete()) {
            return response()->json(['status' => 'success', 'msg' => 'You have successfully deleted Sub-Module']);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error in deleting sub-module. Please try again later!']);
        }
    }

    public function showModule(Request $request)
    {
        $modules = Module::whereProjectId($request->project_id)->get();

        return response()->json(['modules' => $modules]);
    }

    
    public function showSubModule(Request $request)
    {
        $modules = SubModule::whereModuleId($request->module_id)->get();

        return response()->json(['sub_modules' => $modules]);
    }

}

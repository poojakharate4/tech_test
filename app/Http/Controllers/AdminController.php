<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Module;
use App\Models\SubModule;
use App\Models\Project;
use App\Models\Task;

use Session;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index()
    {

        return view('admin.dashboard');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function projects()
    {
        $projects = Project::get();

        return view('admin.project.index', compact('projects'));
    }

    public function addProject(Request $request)
    {
        $employees = Employee::whereHas('roles', function($q){
            $q->where('name', 'user');
        })->get();

        return view('admin.project.add', compact('employees'));
    }


    public function saveProject(Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'members' => ['required', 'array'],
            'type' => ['required']
        ]);

        $formData['members'] =  implode(',',$request->members);

        Project::create($formData);

        return redirect('/projects')->with('success', 'Proejct saved successfully!');
    }


    public function editProject($id, Request $request)
    {
        $employees = Employee::whereHas('roles', function($q){
            $q->where('name', 'user');
        })->get();

        $project = Project::whereId($id)->first();

        return view('admin.project.edit', compact('employees', 'project'));
    }


    public function updateProject($id, Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'members' => ['required', 'array'],
            'type' => ['required']
        ]);

        $formData['members'] =  implode(',',$request->members);
        $formData['last_operation'] = 'E';

        $project = Project::whereId($id)->first();

        $project->update($formData);

        return redirect('/projects')->with('success', 'Project updated successfully!');
    }

    public function deleteProject(Request $request)
    {
        $id = $request->id;
        $project = Project::whereId($id)->first(); 
        $project->last_operation ='D';
        $project->save();

        if ($project->delete()) {
            return response()->json(['status' => 'success', 'msg' => 'You have successfully deleted Project']);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error in deleting Project. Please try again later!']);
        }
    }

    public function modules()
    {
        $modules = Module::get();

        return view('admin.module.index', compact('modules'));
    }

    public function addModule(Request $request)
    {
        $projects = Project::get();

        return view('admin.module.add', compact('projects'));
    }


    public function saveModule(Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'project_id' => ['required'],
        ]);


        Module::create($formData);

        return redirect('/modules')->with('success', 'Module saved successfully!');
    }


    public function editModule($id, Request $request)
    {
        $projects = Project::get();

        $module = Module::whereId($id)->first();

        return view('admin.module.edit', compact('projects', 'module'));
    }


    public function updateModule($id, Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'project_id' => ['required'],
        ]);

        $formData['last_operation'] = 'E';

        $module = Module::whereId($id)->first();

        $module->update($formData);

        return redirect('/modules')->with('success', 'Module updated successfully!');
    }

    public function deleteModule(Request $request)
    {
        $id = $request->id;
        $module = Module::whereId($id)->first(); 
        $module->last_operation ='D';
        $module->save();
        
        if ($module->delete()) {
            return response()->json(['status' => 'success', 'msg' => 'You have successfully deleted Module']);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error in deleting module. Please try again later!']);
        }
    }

    public function subModules()
    {
        $modules = SubModule::get();

        return view('admin.sub-module.index', compact('modules'));
    }

    public function addSubModule(Request $request)
    {
        $projects = Project::get();
        $modules = Module::get();

        return view('admin.sub-module.add', compact('projects', 'modules'));
    }


    public function saveSubModule(Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'project_id' => ['required'],
            'module_id' => ['required'],
        ]);


        SubModule::create($formData);

        return redirect('/sub-modules')->with('success', 'Sub-Module saved successfully!');
    }


    public function editSubModule($id, Request $request)
    {
        $sub_module = SubModule::whereId($id)->first();

        $projects = Project::get();
        $modules = Module::whereProjectId($sub_module->project_id)->get();

        return view('admin.sub-module.edit', compact('projects', 'modules', 'sub_module'));
    }


    public function updateSubModule($id, Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'project_id' => ['required'],
            'module_id' => ['required'],
        ]);

        $formData['last_operation'] = 'E';

        $sub_module = SubModule::whereId($id)->first();

        $sub_module->update($formData);

        return redirect('/sub-modules')->with('success', 'Sub-Module updated successfully!');
    }

    public function deleteSubModule(Request $request)
    {
        $id = $request->id;
        $module = SubModule::whereId($id)->first(); 
        $module->last_operation ='D';
        $module->save();
        
        if ($module->delete()) {
            return response()->json(['status' => 'success', 'msg' => 'You have successfully deleted Sub-Module']);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error in deleting sub-module. Please try again later!']);
        }
    }

    public function showModule(Request $request)
    {
        $modules = Module::whereProjectId($request->project_id)->get();

        return response()->json(['modules' => $modules]);
    }

    
    public function showSubModule(Request $request)
    {
        $modules = SubModule::whereModuleId($request->module_id)->get();

        return response()->json(['sub_modules' => $modules]);
    }

    public function tasks()
    {
        $tasks = Task::get();

        return view('admin.task.index', compact('tasks'));
    }

    public function addTask(Request $request)
    {
        $projects = Project::get();
        $modules = Module::get();

        return view('admin.task.add', compact('projects', 'modules'));
    }


    public function saveTask(Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'project_id' => ['required'],
            'module_id' => ['required'],
            'sub_module_id' => ['required'],
            'start_date' => ['required'],
            'end_date' => ['required'],
            'type' => ['required'],
            'status' => ['required']
        ]);


        Task::create($formData);

        return redirect('/tasks')->with('success', 'task saved successfully!');
    }


    public function editTask($id, Request $request)
    {
        $task = Task::whereId($id)->first();

        $projects = Project::get();
        $modules = Module::whereProjectId($task->project_id)->get();
        $sub_modules = SubModule::whereModuleId($task->module_id)->get();

        return view('admin.task.edit', compact('projects', 'modules', 'sub_modules', 'task'));
    }


    public function updateTask($id, Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'project_id' => ['required'],
            'module_id' => ['required'],
            'sub_module_id' => ['required'],
            'start_date' => ['required'],
            'end_date' => ['required'],
            'type' => ['required'],
            'status' => ['required']
        ]);

        $formData['last_operation'] = 'E';

        $task = Task::whereId($id)->first();

        $task->update($formData);

        return redirect('/tasks')->with('success', 'task updated successfully!');
    }

    public function deleteTask(Request $request)
    {
        $id = $request->id;
        $module = Task::whereId($id)->first(); 
        $module->last_operation ='D';
        $module->save();
        
        if ($module->delete()) {
            return response()->json(['status' => 'success', 'msg' => 'You have successfully deleted task']);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error in deleting task. Please try again later!']);
        }
    }


}

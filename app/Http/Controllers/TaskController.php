<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Module;
use App\Models\SubModule;
use App\Models\Project;
use App\Models\Task;

use Session;

class TaskController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $tasks = Task::get();

        return view('task.index', compact('tasks'));
    }

    public function addTask(Request $request)
    {
        $projects = Project::get();
        $modules = Module::get();

        return view('task.add', compact('projects', 'modules'));
    }


    public function saveTask(Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'project_id' => ['required'],
            'module_id' => ['required'],
            'sub_module_id' => ['required'],
            'start_date' => ['required'],
            'end_date' => ['required'],
            'type' => ['required'],
            'status' => ['required']
        ]);


        Task::create($formData);

        return redirect('/tasks')->with('success', 'task saved successfully!');
    }


    public function editTask($id, Request $request)
    {
        $task = Task::whereId($id)->first();

        $projects = Project::get();
        $modules = Module::whereProjectId($task->project_id)->get();
        $sub_modules = SubModule::whereModuleId($task->module_id)->get();

        return view('task.edit', compact('projects', 'modules', 'sub_modules', 'task'));
    }


    public function updateTask($id, Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'project_id' => ['required'],
            'module_id' => ['required'],
            'sub_module_id' => ['required'],
            'start_date' => ['required'],
            'end_date' => ['required'],
            'type' => ['required'],
            'status' => ['required']
        ]);

        $formData['last_operation'] = 'E';

        $task = Task::whereId($id)->first();

        $task->update($formData);

        return redirect('/tasks')->with('success', 'task updated successfully!');
    }

    public function deleteTask(Request $request)
    {
        $id = $request->id;
        $module = Task::whereId($id)->first(); 
        $module->last_operation ='D';
        $module->save();
        
        if ($module->delete()) {
            return response()->json(['status' => 'success', 'msg' => 'You have successfully deleted task']);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error in deleting task. Please try again later!']);
        }
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\Employee;

use Session;

class ProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $projects = Project::get();

        return view('project.index', compact('projects'));
    }

    public function addProject(Request $request)
    {
        $employees = Employee::whereHas('roles', function($q){
            $q->where('name', 'user');
        })->get();

        return view('project.add', compact('employees'));
    }


    public function saveProject(Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'members' => ['required', 'array'],
            'type' => ['required']
        ]);

        $formData['members'] =  implode(',',$request->members);

        Project::create($formData);

        return redirect('/projects')->with('success', 'Proejct saved successfully!');
    }


    public function editProject($id, Request $request)
    {
        $employees = Employee::whereHas('roles', function($q){
            $q->where('name', 'user');
        })->get();

        $project = Project::whereId($id)->first();

        return view('project.edit', compact('employees', 'project'));
    }


    public function updateProject($id, Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'members' => ['required', 'array'],
            'type' => ['required']
        ]);

        $formData['members'] =  implode(',',$request->members);
        $formData['last_operation'] = 'E';

        $project = Project::whereId($id)->first();

        $project->update($formData);

        return redirect('/projects')->with('success', 'Project updated successfully!');
    }

    public function deleteProject(Request $request)
    {
        $id = $request->id;
        $project = Project::whereId($id)->first(); 
        $project->last_operation ='D';
        $project->save();

        if ($project->delete()) {
            return response()->json(['status' => 'success', 'msg' => 'You have successfully deleted Project']);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error in deleting Project. Please try again later!']);
        }
    }

}

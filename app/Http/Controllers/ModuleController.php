<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Module;
use App\Models\Project;

use Session;

class ModuleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $modules = Module::get();

        return view('module.index', compact('modules'));
    }

    public function addModule(Request $request)
    {
        $projects = Project::get();

        return view('module.add', compact('projects'));
    }


    public function saveModule(Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'project_id' => ['required'],
        ]);


        Module::create($formData);

        return redirect('/modules')->with('success', 'Module saved successfully!');
    }


    public function editModule($id, Request $request)
    {
        $projects = Project::get();

        $module = Module::whereId($id)->first();

        return view('module.edit', compact('projects', 'module'));
    }


    public function updateModule($id, Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'project_id' => ['required'],
        ]);

        $formData['last_operation'] = 'E';

        $module = Module::whereId($id)->first();

        $module->update($formData);

        return redirect('/modules')->with('success', 'Module updated successfully!');
    }

    public function deleteModule(Request $request)
    {
        $id = $request->id;
        $module = Module::whereId($id)->first(); 
        $module->last_operation ='D';
        $module->save();
        
        if ($module->delete()) {
            return response()->json(['status' => 'success', 'msg' => 'You have successfully deleted Module']);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error in deleting module. Please try again later!']);
        }
    }

   
}

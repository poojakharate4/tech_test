@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Project</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form id="appForm"  method="post" autocomplete="off" enctype="multipart/form-data" data-parsley-validate>
                        @csrf
                        <div class="mb-3">
                            <label for="name" class="form-label">Name<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="name" name="name" required  data-parsley-required-message="Please enter name." 
                            value="{{old('name', $project->name)}}">
                            @if($errors->has('name'))
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                       
                        <div class="mb-3">
                            <label for="cat" class="form-label">Employees<span class="text-danger">*</span></label>
                            <select class="form-select" id="cat" aria-label="Default select example" name="members[]" multiple
                            required  data-parsley-required-message="Please select Employees.">
                                <option value="">Select Employees</option>
                                @foreach($employees as $key=> $emp)
                                    <option value="{{$emp->id}}" @if(old('members') != null && in_array($emp->id, old('members')) || in_array($emp->id, explode(',', $project->members)))  selected @endif>{{$emp->name}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('members'))
                                <span class="text-danger">{{ $errors->first('members') }}</span>
                            @endif
                        </div>

                        <div class="mb-3">
                            <label for="type" class="form-label">Type<span class="text-danger">*</span></label>
                            <select class="form-select" id="type" name="type" aria-label="Default select example"
                            required  data-parsley-required-message="Please select Type.">
                                <option  value="" selected>Select Type</option>
                                <option value="project" @if(old('type', $project->type) == 'project') selected @endif>Project</option>
                                <option value="CRM" @if(old('type', $project->type) == 'CRM') selected @endif>CRM</option>
                            </select>
                            @if($errors->has('type'))
                                <span class="text-danger">{{ $errors->first('type') }}</span>
                            @endif
                        </div>

                        <button type="submit" class="btn btn-primary" >Submit</button>
                    </form>
                
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
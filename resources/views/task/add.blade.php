@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add Task</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form id="appForm"  method="post" autocomplete="off" enctype="multipart/form-data" data-parsley-validate>
                        @csrf
                        <div class="mb-3">
                            <label for="name" class="form-label">Name<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="name" name="name" required  data-parsley-required-message="Please enter name." value="{{old('name')}}">
                            @if($errors->has('name'))
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                       
                        <div class="mb-3">
                            <label for="cat" class="form-label">Project<span class="text-danger">*</span></label>
                            <select class="form-select" id="cat" aria-label="Default select example" name="project_id" 
                            required  data-parsley-required-message="Please select Project." onchange="showModule($(this).val());">
                                <option value="" selected>Select Project</option>
                                @foreach($projects as $key=> $project)
                                    <option value="{{$project->id}}" @if(old('project_id') == $project->id) selected @endif>{{$project->name}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('project_id'))
                                <span class="text-danger">{{ $errors->first('project_id') }}</span>
                            @endif
                        </div>

                        <div class="mb-3" >
                            <label for="cat" class="form-label">Module<span class="text-danger">*</span></label>
                            <select class="form-select" aria-label="Default select example" name="module_id" 
                            required  data-parsley-required-message="Please select Module." id="moduleId"
                            onchange="showSubModule($(this).val());">
                            <option value="">Select</option>
                            </select>
                            @if($errors->has('module_id'))
                                <span class="text-danger">{{ $errors->first('module_id') }}</span>
                            @endif
                        </div>

                        <div class="mb-3" >
                            <label for="cat" class="form-label">Sub-Module<span class="text-danger">*</span></label>
                            <select class="form-select" aria-label="Default select example" name="sub_module_id" 
                            required  data-parsley-required-message="Please select Sub-Module." id="subModuleId">
                            <option value="">Select</option>
                            </select>
                            @if($errors->has('sub_module_id'))
                                <span class="text-danger">{{ $errors->first('sub_module_id') }}</span>
                            @endif
                        </div>
                        
                        <div class="mb-3" >
                            <label for="cat" class="form-label">Start Date<span class="text-danger">*</span></label>
                            <input type="date" class="form-control" id="start_date" name="start_date" required  data-parsley-required-message="Please enter start date." value="{{old('start_date')}}">
                            @if($errors->has('start_date'))
                                <span class="text-danger">{{ $errors->first('start_date') }}</span>
                            @endif
                        </div>
                        <div class="mb-3" >
                            <label for="cat" class="form-label">End Date<span class="text-danger">*</span></label>
                            <input type="date" class="form-control" id="end_date" name="end_date" required  data-parsley-required-message="Please enter end date." value="{{old('end_date')}}">
                            @if($errors->has('end_date'))
                                <span class="text-danger">{{ $errors->first('end_Date') }}</span>
                            @endif
                        </div>
                        <div class="mb-3">
                            <label for="type" class="form-label">Type<span class="text-danger">*</span></label>
                            <select class="form-select" id="type" name="type" aria-label="Default select example"
                            required  data-parsley-required-message="Please select Type.">
                                <option  value="" >Select Type</option>
                                <option value="General" @if(old('type') == 'General') selected @endif>General</option>
                                <option value="Development" @if(old('type') == 'Development') selected @endif>Development</option>
                                <option value="Bug" @if(old('type') == 'Bug') selected @endif>Bug</option>
                                <option value="Chnage Request" @if(old('type') == 'Chnage Request') selected @endif>Chnage Request</option>
                            </select>
                            @if($errors->has('type'))
                                <span class="text-danger">{{ $errors->first('type') }}</span>
                            @endif
                        </div>
                        <div class="mb-3">
                            <label for="type" class="form-label">Status<span class="text-danger">*</span></label>
                            <select class="form-select" id="status" name="status" aria-label="Default select example"
                            required  data-parsley-required-message="Please select status.">
                                <option  value="" >Select status</option>
                                <option value="todo" @if(old('status') == 'todo') selected @endif>{{ucfirst('todo')}}</option>
                                <option value="running" @if(old('status') == 'running') selected @endif>{{ucfirst('running')}}</option>
                                <option value="complete" @if(old('status') == 'complete') selected @endif>{{ucfirst('complete')}}</option>

                            </select>
                            @if($errors->has('status'))
                                <span class="text-danger">{{ $errors->first('status') }}</span>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary" >Submit</button>
                    </form>
                
                </div>
            </div>
        </div>
    </div>
</div>
<script>
  function showModule(id) {
        $.ajax({
            type: "get",
            url: "{{ url('/show-module') }}",
            data: {
                "_token": "{{ csrf_token() }}",
                "project_id": id
            },
            success: function (data) {

                var select = ' <option value="">Select Module</option>';
                $.each(data.modules, function (key, value) {
                    console.log(value)
                    select += '<option value=' + value.id + '>' + value.name + '</option>';
                });
                $('#moduleId').html(select);
            }
        });
    }

    function showSubModule(id) {
        $.ajax({
            type: "get",
            url: "{{ url('/show-sub-module') }}",
            data: {
                "_token": "{{ csrf_token() }}",
                "module_id": id
            },
            success: function (data) {

                var select = ' <option value="">Select Sub-Module</option>';
                $.each(data.sub_modules, function (key, value) {
                    console.log(value)
                    select += '<option value=' + value.id + '>' + value.name + '</option>';
                });
                $('#subModuleId').html(select);
            }
        });
    }
</script>
@endsection
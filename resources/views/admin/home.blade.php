@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <a href="{{url('projects')}}" class="btn btn-success">Projects </a>
                    <a href="{{url('modules')}}" class="btn btn-success">Modules </a>
                    <a href="{{url('sub-modules')}}" class="btn btn-success">SubModules </a>
                    <a href="{{url('tasks')}}" class="btn btn-success">Tasks </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<script>
    

    function showData(page)
    {
        $.ajax({
            type: "get",
            url: "{{ url('/show-data') }}",
            data: {
                "page" : page
            },
            success: function (data) {
                $('#appData').html(data);
                $('#joinTeam').hide();
                $('appForm').parsley();
            }
        });
    }

    function showSkill()
    {
        id = $('#cat').val();
        
        $.ajax({
            type: "get",
            url: "{{ url('/show-skill') }}",
            data: {
                "id": id
            },
            success: function (data) {
                var select = ' <option value="" selected>Select Skill</option>';
                $.each(data.skills, function (key, value) {
                    select += '<option value=' + value.id + '>' + value.name + '</option>';
                });
                $('#skill').attr('required','');
                $('#skill').attr('data-parsley-required-message','Please select Skills.');
                $('#skill').html(select);
                $('#skillSet').show();
            }
        });
    }

</script>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Manage Sub-Modules</div>
                <div class="col-sm-6 text-right">
                <a href="{{url('/')}}" class="btn btn-success ">Home Page</a>
                    <a href="{{url('add-sub-module')}}" class="btn btn-success ">Add Sub-Module</a>
                </div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="table-responsive">
                    <table class="table" id="example1">
                        <thead>
                        <tr>
                            <th class="column-title">Created At </th>
                            <th class="column-title">Name </th>
                            <th class="column-title">Project </th>
                            <th class="column-title">Module </th>
                            <th class="column-title text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1;  ?>
                        @foreach($modules as $row)
                            <tr>
                                <td>{{ @$row->created_at }}</td>
                                <td>{{ @$row->name }}</td>
                                <td>
                                  {{@$row->project->name}}
                                </td>
                                <td>
                                  {{@$row->module->name}}
                                </td>
                                <td class="text-center">
                                    <a href="{{URL::to('/edit-sub-module',['id'=>@$row->id])}}" title="Edit">Edit</a>
                                    
                                    <a href="javascript:void(0);" class="deleteModule" id="{{@$row->id}}" title="Delete">Delete</a>
                                </td>

                            </tr>
                            <?php $i++; ?>
                        @endforeach
                        
                        </tbody>
                    </table>
                </div>
              
                </div>
            </div>
        </div>
    </div>
</div>
<script>
     $(document).ready(function () {

$("#example1").on("click", ".deleteModule", function (e) {

    e.preventDefault();
    let id = $(this).attr('id');
    swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Sub-Module!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "post",
                    url: "{{ url('/delete-sub-module') }}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id": id
                    },
                    success: function (response) {

                        if (response.status == "success") {
                            toastr.success(response.msg);

                            setTimeout(function () {
                                location.reload();
                            }, 5000)

                        }
                        if (response.status == "error") {
                            toastr.info(response.msg);
                            setTimeout(function () {
                                location.reload();
                            }, 5000)
                        }
                    }
                });
                swal("Deleted!", "Sub-Module deleted successfully.", "success");
            } else {
                swal("Cancelled", "Sub-Module is safe :)", "error");
            }
        });
});
});
</script>
@endsection


@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Sub-Module</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form id="appForm"  method="post" autocomplete="off" enctype="multipart/form-data" data-parsley-validate>
                        @csrf
                        <div class="mb-3">
                            <label for="name" class="form-label">Name<span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="name" name="name" required  data-parsley-required-message="Please enter name."
                             value="{{old('name', $sub_module->name)}}">
                            @if($errors->has('name'))
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                       
                        <div class="mb-3">
                            <label for="cat" class="form-label">Project<span class="text-danger">*</span></label>
                            <select class="form-select" id="cat" aria-label="Default select example" name="project_id" 
                            required  data-parsley-required-message="Please select Project." onchange="showModule($(this).val());">
                                <option value="" selected>Select Project</option>
                                @foreach($projects as $key=> $project)
                                    <option value="{{$project->id}}" @if(old('project_id', $sub_module->project_id) == $project->id) selected @endif>{{$project->name}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('project_id'))
                                <span class="text-danger">{{ $errors->first('project_id') }}</span>
                            @endif
                        </div>

                        <div class="mb-3" >
                            <label for="cat" class="form-label">Module<span class="text-danger">*</span></label>
                            <select class="form-select" aria-label="Default select example" name="module_id" 
                            required  data-parsley-required-message="Please select Module." id="moduleId">
                            <option value="">Select Module</option>
                            @foreach($modules as $key=> $module)
                                    <option value="{{$module->id}}" @if(old('module_id', $sub_module->module_id) == $module->id) selected @endif>{{$module->name}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('module_id'))
                                <span class="text-danger">{{ $errors->first('module_id') }}</span>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary" >Submit</button>
                    </form>
                
                </div>
            </div>
        </div>
    </div>
</div>
<script>
  function showModule(id) {
        $.ajax({
            type: "get",
            url: "{{ url('/show-module') }}",
            data: {
                "_token": "{{ csrf_token() }}",
                "project_id": id
            },
            success: function (data) {

                var select = ' <option value="">Select Module</option>';
                $.each(data.modules, function (key, value) {
                    console.log(value)
                    select += '<option value=' + value.id + '>' + value.name + '</option>';
                });
                $('#moduleId').html(select);
            }
        });
    }
</script>
@endsection
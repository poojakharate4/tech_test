<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\models\Employee;

use Hash;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        collect([
            [ 
                'name' => 'Admin User',
                'email' => 'admin@test.com',
                'phone_number' => '7974912168',
                'password' => Hash::make('Test@123#'),
            ],
            [  
                'name' => 'employee 1',
                'email' => 'employee1@yopmail.com',
                'phone_number' => '7974912168',
                'password' => Hash::make('Test@456#'),
            ],
            [  
                'name' => 'employee 2',
                'email' => 'employee2@yopmail.com',
                'phone_number' => '7974912168',
                'password' => Hash::make('Test@789#'),
            ],
            [  
                'name' => 'employee 3',
                'email' => 'employee3@yopmail.com',
                'phone_number' => '7974912168',
                'password' => Hash::make('Test@789#'),
                'created_at' => date("Y-m-d h:i:s")
            ],
            [  
                'name' => 'employee 4',
                'email' => 'employee4@yopmail.com',
                'phone_number' => '7974912168',
                'password' => Hash::make('Test@789#'),
                'created_at' => date("Y-m-d h:i:s")
            ]

        ])->each(function ($employee) {

           $newemployee = Employee::create($employee);

           $role = $employee['name'] == 'Admin User' ? 'admin' : 'user';

           $newemployee->assignRole($role);
        });
    }
}

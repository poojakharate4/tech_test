<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Auth::routes();


Route::middleware('auth')->group(function () {

    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    //project routes
    Route::get('/projects', 'App\Http\Controllers\ProjectController@index');
    Route::get('/add-project', 'App\Http\Controllers\ProjectController@addProject');
    Route::post('/add-project', 'App\Http\Controllers\ProjectController@saveProject');
    Route::get('/edit-project/{id}', 'App\Http\Controllers\ProjectController@editProject');
    Route::post('/edit-project/{id}', 'App\Http\Controllers\ProjectController@updateProject');
    Route::post('/delete-project', 'App\Http\Controllers\ProjectController@deleteProject');

    //module routes
    Route::get('/modules', 'App\Http\Controllers\ModuleController@index');
    Route::get('/add-module', 'App\Http\Controllers\ModuleController@addModule');
    Route::post('/add-module', 'App\Http\Controllers\ModuleController@saveModule');
    Route::get('/edit-module/{id}', 'App\Http\Controllers\ModuleController@editModule');
    Route::post('/edit-module/{id}', 'App\Http\Controllers\ModuleController@updateModule');
    Route::post('/delete-module', 'App\Http\Controllers\ModuleController@deleteModule');

    //sub-module routes
    Route::get('/sub-modules', 'App\Http\Controllers\SubModuleController@index');
    Route::get('/add-sub-module', 'App\Http\Controllers\SubModuleController@addSubModule');
    Route::post('/add-sub-module', 'App\Http\Controllers\SubModuleController@saveSubModule');
    Route::get('/edit-sub-module/{id}', 'App\Http\Controllers\SubModuleController@editSubModule');
    Route::post('/edit-sub-module/{id}', 'App\Http\Controllers\SubModuleController@updateSubModule');
    Route::post('/delete-sub-module', 'App\Http\Controllers\SubModuleController@deleteSubModule');
    Route::get('/show-module', 'App\Http\Controllers\SubModuleController@showModule');
    Route::get('/show-sub-module', 'App\Http\Controllers\SubModuleController@showSubModule');

    //task routes
    Route::get('/tasks', 'App\Http\Controllers\TaskController@index');
    Route::get('/add-task', 'App\Http\Controllers\TaskController@addTask');
    Route::post('/add-task', 'App\Http\Controllers\TaskController@saveTask');
    Route::get('/edit-task/{id}', 'App\Http\Controllers\TaskController@editTask');
    Route::post('/edit-task/{id}', 'App\Http\Controllers\TaskController@updateTask');
    Route::post('/delete-task', 'App\Http\Controllers\TaskController@deleteTask');
   


});

Route::prefix('admin')->group(function() {
    Route::get('login','App\Http\Controllers\Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('login', 'App\Http\Controllers\Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('logout', 'App\Http\Controllers\Auth\AdminLoginController@logout')->name('admin.logout');

    Route::get('/', 'App\Http\Controllers\AdminController@index')->name('admin.dashboard');
    
    //project routes
    Route::get('/projects', 'App\Http\Controllers\AdminController@projects');
    Route::get('/add-project', 'App\Http\Controllers\AdminController@addProject');
    Route::post('/add-project', 'App\Http\Controllers\AdminController@saveProject');
    Route::get('/edit-project/{id}', 'App\Http\Controllers\AdminController@editProject');
    Route::post('/edit-project/{id}', 'App\Http\Controllers\AdminController@updateProject');
    Route::post('/delete-project', 'App\Http\Controllers\AdminController@deleteProject');

    //module routes
    Route::get('/modules', 'App\Http\Controllers\AdminCOntroller@modules');
    Route::get('/add-module', 'App\Http\Controllers\AdminCOntroller@addModule');
    Route::post('/add-module', 'App\Http\Controllers\AdminCOntroller@saveModule');
    Route::get('/edit-module/{id}', 'App\Http\Controllers\AdminCOntroller@editModule');
    Route::post('/edit-module/{id}', 'App\Http\Controllers\AdminCOntroller@updateModule');
    Route::post('/delete-module', 'App\Http\Controllers\AdminCOntroller@deleteModule');

    //sub-module routes
    Route::get('/sub-modules', 'App\Http\Controllers\AdminController@subModules');
    Route::get('/add-sub-module', 'App\Http\Controllers\AdminController@addSubModule');
    Route::post('/add-sub-module', 'App\Http\Controllers\AdminController@saveSubModule');
    Route::get('/edit-sub-module/{id}', 'App\Http\Controllers\AdminController@editSubModule');
    Route::post('/edit-sub-module/{id}', 'App\Http\Controllers\AdminController@updateSubModule');
    Route::post('/delete-sub-module', 'App\Http\Controllers\AdminController@deleteSubModule');
    Route::get('/show-module', 'App\Http\Controllers\AdminController@showModule');
    Route::get('/show-sub-module', 'App\Http\Controllers\AdminController@showSubModule');

    //task routes
    Route::get('/tasks', 'App\Http\Controllers\AdminController@tasks');
    Route::get('/add-task', 'App\Http\Controllers\AdminController@addTask');
    Route::post('/add-task', 'App\Http\Controllers\AdminController@saveTask');
    Route::get('/edit-task/{id}', 'App\Http\Controllers\AdminController@editTask');
    Route::post('/edit-task/{id}', 'App\Http\Controllers\AdminController@updateTask');
    Route::post('/delete-task', 'App\Http\Controllers\AdminController@deleteTask');
});
